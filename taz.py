#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 11:23:14 2017

@author: The RocketMan
"""
from bs4 import BeautifulSoup as Soup
from selenium import webdriver
import time
from urllib.request import urlopen as uReq
import urllib.request
import urllib
import pandas as pd

final = []

driver = webdriver.Chrome(executable_path = '/home/fractaluser/bin')
driver.get('http://www.taz.de/Oeko/!p4623/')

html_source = driver.page_source
page_soup = Soup(html_source,"html.parser")


hcontainers = page_soup.findAll("ul",{"role":"directory"})
try:
    for l in hcontainers:
        temp = l.findAll('li')
        for t in temp:
            #print(t.a.get('href'))
            my_Url = "https://www.taz.de" + t.a.get('href')
            print(my_Url)
            tmp = urllib.request.Request(my_Url)
            uClient = uReq(tmp)
            #time.sleep(120)
            page_html = uClient.read()
            uClient.close()
            #driver.get(my_Url)
            #driver.find_element_by_class_name('tzi-paywahl__close').click()
            
            tmp = urllib.request.Request(my_Url)
            uClient = uReq(tmp)
            page_html = uClient.read()
            uClient.close()
            
            page_soup = Soup(page_html,"html.parser")             
            
            hcontainers = page_soup.find("div",{"class":"sectbody"})
            
            headlines = hcontainers.h1.text
            #print("HEADLINES " + headlines)
            containers = hcontainers.findAll("p")
            article = ''
            for l in containers:
                article = article + l.text
            #print("ARTICLE " + article)
            final.append({'Url':my_Url,'Headlines':headlines,'Article':article})
except Exception as e:
    print(str(e))
    pass

pd.DataFrame.from_dict(final).to_csv('taz.csv')






def distance_from_zero(s):
    if str(type(s)) in {"<class 'int'>","<class 'float'>"}:
        return abs(s)
    else:
        return "Nope"

#INDIVIDUAL LINK CODE BELOW
driver.get('http://www.taz.de/Markenrechte-an-Alnatura/!5424632/')
driver.find_element_by_class_name('tzi-paywahl__close').click()

my_Url = 'http://www.taz.de/Markenrechte-an-Alnatura/!5424632/'
tmp = urllib.request.Request(my_Url)
uClient = uReq(tmp)
page_html = uClient.read()
uClient.close()

page_soup = Soup(page_html,"html.parser")             

hcontainers = page_soup.find("div",{"class":"sectbody"})

headlines = hcontainers.h1.text

containers = hcontainers.findAll("p")
article = ''
for l in containers:
    article = article + l.text
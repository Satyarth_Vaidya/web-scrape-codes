#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:33:39 2017
@author: Satyarth Vaidya
"""
from bs4 import BeautifulSoup as Soup
from selenium import webdriver
from urllib.request import urlopen as uReq
import urllib.request
import urllib
import pandas as pd

final = []
cfinal = []
driver = webdriver.Chrome(executable_path = '/home/fractaluser/bin')

for pg in range(1,20):

    try:
        print('https://simple-value-investing.de/?pg=' + str(pg))
        driver.get('http://simple-value-investing.de/?pg=' + str(pg))
        #driver.get('http://simple-value-investing.de/?pg=15')
    
        html_source = driver.page_source
        page_soup = Soup(html_source,"html.parser")    
        blog_link = page_soup.findAll("h1",{"class":"entry-title"})
    
        for l in blog_link:
            print(l.a.get('href'))
            my_Url = l.a.get('href')
            tmp = urllib.request.Request(my_Url)
            uClient = uReq(tmp)
            #time.sleep(120)
            page_html = uClient.read()
            uClient.close()
            
            page_soup_ = Soup(page_html,"html.parser")
            
            btitle = page_soup_.find("h1",{"class":"entry-title"})
            content_ = page_soup_.findAll("div",{"class":"entry-content"})
            
            #comment= page_soup_.findAll("ol",{"class":"comments"})
#            if len(comment) == 0:
#                continue
            #lists = comment[0].find_all('li')
            
            c = content_[0].findAll('p')
            t=''
            for i in c:
               t = t + i.text
            #print(t)
#            final.append({'URL':my_Url,'TITLE:':btitle.text,'CONTENT':t,'Type':'a'})
#            final.append({'Url':my_Url,'Headlines':btitle.text,'Article':t})
            final.append({'Url':my_Url,'Headlines':btitle.text,'Article':t})
    except Exception as e:
        print(str(e))
        pass      
            
#            for ele in lists:
#    #           comments_ = comment.findAll('p')        
##                final.append({'URL':my_Url,'TITLE:':btitle.text,'CONTENT':,'Type':'c'})
#                cfinal.append({'Url':my_Url,'Headlines':btitle.text,'Article':ele.text.split("\n")})

    #           cfinal.append({"URL":my_Url,"TITLE:":btitle.text,"BLOG":comment[0].get_text().split("--")})
    #           tag = 1
    #           for tag in content_[0].find_all('p'):
    #              content = content+' '+tag.text
    #           print("TITLE : " + btitle.text + "\n")
    #           print("CONTENT :" + content)
                #content = ele.get_text()
                #print(content)
                #print("\n")
    

pd.DataFrame.from_dict(cfinal).to_csv('Comments_Blog.csv')
pd.DataFrame.from_dict(final).to_csv('Articles.csv',index = 'False' )

##INDIVIDUAL LINK CODE BELOW 

my_Url = 'http://simple-value-investing.de/blog/fundamentaldatenuebersicht-deutscher-unternehmen-als-buch'
print(my_Url + "\n")
tmp = urllib.request.Request(my_Url)
uClient = uReq(tmp)
#time.sleep(120)
page_html = uClient.read()
uClient.close()

page_soup_ = Soup(page_html,"html.parser")

btitle = page_soup_.find("h1",{"class":"entry-title"})
content_ = page_soup_.findAll("div",{"class":"entry-content"})

comment= page_soup_.findAll("ol",{"class":"comments"})
comments_ = comment.findAll('p')

tag = 1

#for tag in content_[0].find_all('p'):
#   content = ""
#   #content = content+' '+tag.l.text
c = content_[0].findAll('p')
t=''
for i in c:
   t = t + i.text
print(t)


print("TITLE : " + btitle.text + "\n")
#print("CONTENT :" + content)
comment[0].get_text().split("\n")

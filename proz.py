#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  3 17:44:19 2017

@author: Satyarth Vaidya
"""

from bs4 import BeautifulSoup as Soup
from selenium import webdriver
import time
from urllib.request import urlopen as uReq
import urllib

terms=[]
driver = webdriver.Chrome(executable_path = '/home/fractaluser/bin')
for pg in range(1,65):
    #driver.get("http://www.proz.com/glossary-translations/german-to-english-translations/48/Finance-%28general%29")
    driver.get("http://www.proz.com/glossary-translations/german-to-english-translations/48/page" + str(pg))
    
    html_source = driver.page_source
    page_soup = Soup(html_source,"html.parser")
    
    #containers = page_soup.find_all("table",{"class":"table"})
    #[w.replace('\n\n\n\n\n\n\n', ' ') for w in containers[0].get_text()]
    
    #b = containers[0].get_text().replace('\n','')
    #b.replace('   ',' ')
    
    d = page_soup.find_all("tr")
    d[3].find('td').get_text()
    
    
    i=0
    for l in d:
        if(i%2 == 1):
            #print(l.find('td').get_text().strip())
            terms.append(l.find('td').get_text().strip())
        i=i+1

pd.DataFrame.from_dict(terms).to_csv('German_Terms.csv')
